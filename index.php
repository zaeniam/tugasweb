<?php

if(isset($_POST['submit'])){
$nama   =$_POST['nama'];
$matkul =$_POST['matkul'];
$tugas  =$_POST['tugas'];
$uts    =$_POST['uts'];
$uas    =$_POST['uas'];

$nilai_tugas  = $tugas *0.15;
$nilai_uts    = $uts  *0.35;
$nilai_uas    = $uas  *0.50;

$nilai_akhir = $nilai_tugas + $nilai_uts + $nilai_uas;

if ($nilai_akhir >= 90 && $nilai_akhir <= 100) {
    $grade  = "A";
} elseif ($nilai_akhir > 70 && $nilai_akhir < 90) {
    $grade  = "B";
} elseif ($nilai_akhir > 50 && $nilai_akhir <= 70){
    $grade  = "C";
}else{
    $grade  = "D";
}

}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Zaeni Abdul Majid</title>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 border border-primary mt-3">
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Mahasiswa</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="mb-3">
                        <label for="matkul" class="form-label">Mata Kuliah</label>
                        <input type="text" name="matkul" class="form-control" id="matkul">
                    </div>
                    <div class="mb-3">
                        <label for="tugas" class="form-label">Nilai Tugas</label>
                        <input type="text" name="tugas" class="form-control" id="tugas">
                    </div>
                    <div class="mb-3">
                        <label for="uts" class="form-label">Nilai UTS</label>
                        <input type="text" name="uts" class="form-control" id="uts">
                    </div>
                    <div class="mb-3">
                        <label for="uas" class="form-label">Nilai UAS</label>
                        <input type="text" name="uas" class="form-control" id="uas">
                    </div>
                    <button type="submit" name="submit" value="sumbit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <?php if (isset($_POST['submit'])) : ?>
        <div class="row justify-content-center">
            <div class="col-8 border border-primary mt-3 p-3">
                <div class="alert alert-success">
                    Nama        : <?php echo $nama ?> <br>
                    Mata Kuliah : <?php echo $matkul ?> <br>
                    Niali Tugas : <?php echo $tugas ?> <br>
                    Nilai UTS   : <?php echo $uts ?> <br>
                    Nilai UAS   : <?php echo $uas ?> <br>
                    Nilai Total : <?php echo $nilai_akhir ?> <br>
                    Grade       : <?php echo $grade ?> <br>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

   
  </body>
</html>